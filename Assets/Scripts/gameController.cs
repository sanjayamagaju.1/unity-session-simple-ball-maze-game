using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameController : MonoBehaviour
{
    // GameObject ball_obj;
    public float speed;
    private float dirX, dirZ, dirY;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // ball_obj = GameObject.FindGameObjectWithTag("ball");
        // Debug.Log(ball_obj);
        float x_pos = transform.position.x;
        float y_pos = transform.position.y;
        float z_pos = transform.position.z;

        bool jump = Input.GetKey(KeyCode.Space);
        dirX = Input.GetAxis("Horizontal");
        dirZ = Input.GetAxis("Vertical");
        transform.position = new Vector3(dirX * speed, y_pos, dirZ * speed);
        

        if (jump)
        {
            Debug.Log("space entered");
            transform.position =  new Vector3(x_pos, 5f, z_pos);
        }
    }
}
